import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bestiemall';
  logo: any = "../assets/images/logo/bestimall.png";
  gambar1:any = "../assets/images/carousel/gambar-1.png";
  gambar2:any = "../assets/images/carousel/gambar-2.png";
  gambar3:any = "../assets/images/carousel/gambar-3.png"
}
